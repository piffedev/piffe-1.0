# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

School.create!( :name => 'evergreen elementary', :city => 'san jose', :state => 'ca', :zipcode => '95135', :country =>'us')
School.create!( :name => 'james franklin smith elementary', :city => 'san jose', :state => 'ca', :zipcode => '95111', :country =>'us')
