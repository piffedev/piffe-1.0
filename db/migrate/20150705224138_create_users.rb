class CreateUsers < ActiveRecord::Migration
  def change

    create_table :schools do |t|
      t.string :name, index: true
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :country, :default => "us"
    end

    create_table :users do |t|

      t.string :username, :index => true, unique: true
      t.string :password_digest
      t.string :first_name
      t.string :last_name
      t.string :email, index: true, unique: true
      t.belongs_to :school, index: true
      t.string :role, index: true, :null => false  #student, instructor, admin
      t.integer :level, index:  true, :default => 0
      t.timestamp :last_login, null: false
      t.boolean :is_locked, default: false, index: true

      t.timestamps null: false
    end

  end
end