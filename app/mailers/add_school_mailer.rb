class AddSchoolMailer < ApplicationMailer
  @no_reply = 'no-reply@payitforward4edu.com'
  default from: @no_reply

  def add_school_email(add_school)
    @add_school = add_school

    @to_email = Rails.application.config.x.contact_us_mailer.to_list
    @bcc_email = Rails.application.config.x.contact_us_mailer.bcc_list
    @reply_to = @no_reply
    @subject = '[New School] ' + ' New school needs to be added'


    mail(to: @to_email, subject: @subject, reply_to: @reply_to, bcc: @bcc_email)

  end
end
