class ContactUsMailer < ApplicationMailer
  default from: 'no-reply@payitforward4edu.com'

  def contact_us_email(contact_us)
    @contact_us = contact_us

    @to_email = Rails.application.config.x.contact_us_mailer.to_list
    @bcc_email = Rails.application.config.x.contact_us_mailer.bcc_list
    @reply_to = %("#{@contact_us.from_name}" <#{@contact_us.from_email}>)
    @subject = '[Contact Us] ' + @contact_us.subject


    mail(:to => @to_email, :subject => @subject, :reply_to => @reply_to, :bcc => @bcc_email)

  end
end
