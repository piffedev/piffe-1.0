module ApplicationHelper
  # Returns the full title on a per-page basis.

  ZOOM_URL = "https://www.zoom.us"
  SMILE_AMAZON_URL = "http://smile.amazon.com"
  PIFFE_SMILE_AMAZON_URL = "https://smile.amazon.com/ch/47-1723006"

  def full_title(page_title = '')
    base_title = "Pay It Forward For Education (PIFFE)"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
end
