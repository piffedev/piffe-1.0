class School < ActiveRecord::Base
  before_save { name.downcase! }
  before_save { city.downcase! }
  before_save { state.downcase! }

  validates :name, presence: true, length: { maximum: 100 }, uniqueness: {
              scope: :zipcode,
              message: "School already exists"
                 }
  validates :city, presence: true, length: { maximum: 100 }
  validates :state, length: { maximum: 2}
  validates :zipcode, presence: true, length: { minimum: 5, maximum: 5}
end
