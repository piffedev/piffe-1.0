class AddSchool
  include ActiveModel::Model

  attr_accessor :username
  attr_accessor :user_id
  attr_accessor :name
  attr_accessor :city
  attr_accessor :state
  attr_accessor :zipcode


  def clear
    self.username = ''
    self.user_id = ''
    self.name = ''
    self.city = ''
    self.state = ''
    self.zipcode = ''
  end
end