class ContactUs
  include ActiveModel::Model

  attr_accessor :from_name
  attr_accessor :from_email
  attr_accessor :subject
  attr_accessor :message

  #validates :email, presence: true, length: {in:2..255}

  def clear
    self.from_name = ''
    self.from_email = ''
    self.subject = ''
    self.message = ''
  end
end