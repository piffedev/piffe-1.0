class ContactUsController < ApplicationController
  include ActionView::Helpers::TextHelper


  def new
    @contact_us = ContactUs.new
  end

  def create
    @contact_us = ContactUs.new(contact_us_params)
#    @contact_us.message = params[:message]

     @contact_us.message = simple_format(@contact_us.message)

    begin
      ContactUsMailer.contact_us_email(@contact_us).deliver_now
      flash.now[:success] = 'Thank you for your comment, ' + @contact_us.from_name + '. We\'ll contact you as soon as possible.'
    rescue Exception => e
      errors = [@contact_us.from_name + ', our email system is down at the moment. Please try again later. If problem persists, please contact administrator@payitforward4edu.com.']
      errors << e.message
      flash.now[:danger] = errors.join("<br>").html_safe
    end


    @contact_us.clear
    render 'new'

  end



  private # Internal only

  def contact_us_params
    params.require(:contact_us).permit(:from_name, :from_email, :subject, :message)
  end

end
