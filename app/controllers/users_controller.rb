class UsersController < ApplicationController
  @@user_roles =  { "student" => "student", "admin" => "admin", "instructor" => "instructor" }
  @@state_abbreviations = ['AL',
                          'AK',
                          'AZ',
                          'AR',
                          'CA',
                          'CO',
                          'CT',
                          'DE',
                          'DC',
                          'FL',
                          'GA',
                          'HI',
                          'ID',
                          'IL',
                          'IN',
                          'IA',
                          'KS',
                          'KY',
                          'LA',
                          'ME',
                          'MT',
                          'NE',
                          'NV',
                          'NH',
                          'NJ',
                          'NM',
                          'NY',
                          'NC',
                          'ND',
                          'OH',
                          'OK',
                          'OR',
                          'MD',
                          'MA',
                          'MI',
                          'MN',
                          'MS',
                          'MO',
                          'PA',
                          'RI',
                          'SC',
                          'SD',
                          'TN',
                          'TX',
                          'UT',
                          'VT',
                          'VA',
                          'WA',
                          'WV',
                          'WI',
                          'WY'
  ]

  def show
    #debugger
    @user = User.find(params[:id])
    if !@user.school_id.nil?
      @school = School.find(@user.school_id)
    end
  end

  def new
    @user = User.new
    @school_options = getSchools

    @add_school = AddSchool.new
  end

  def create
    @user = User.new(user_params)
    @user.role = @@user_roles["student"]
    if @school_options.nil?
      @school_options = getSchools
    end

    # Same as     if not params[:user][:school_id].blank?
    #unless params[:user][:school_id].blank?
    #  @user.school_id = params[:school_id]
    #end
    @user.last_login = Time.now().utc

    if @user.save
      if params[:user][:school_id].blank?
        # get the data in params[:school] and  and send email with user id
        @add_school = AddSchool.new
        @add_school.username =  @user.username
        @add_school.user_id = @user.id;
        @add_school.name = params[:school][:name]
        @add_school.city = params[:school][:city]
        @add_school.state = params[:school_state]
        @add_school.zipcode = params[:school][:zipcode]
        AddSchoolMailer.add_school_email(@add_school).deliver_now

      end
      flash[:success] = 'Welcome to the PIFFE!'
      redirect_to @user #redirect_to user_url(@user)
      #redirect_to root_path
    else
      # Need to redirect to the user page when it's available:      redirect_to @user #redirect_to user_url(@user)
      render 'new'


    end
  end


  helper_method :state_abbreviations
  # accessing global variable from view
  def state_abbreviations
    @@state_abbreviations
  end


  private # Internal only for below

  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :email, :password,
                                 :password_confirmation, :school_id)
  end


  def getSchools
    School.order(:name).collect{|school| ["#{school.name.titleize}, #{school.city.titleize}, #{school.state.upcase} #{school.zipcode}", school.id]}
  end

end
