class StaticPagesController < ApplicationController
  def home
  end

  def schedule
  end

  def team
  end

  def faq
  end

  def about
  end

  def ping
    render :text => 'pong'
  end


end
