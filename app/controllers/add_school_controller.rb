class AddSchoolController < ApplicationController
  include ActionView::Helpers::TextHelper


  def new
    @add_school = AddSchool.new
  end

  def create
    @add_school = AddSchool.new(add_school_params)
#    @contact_us.message = params[:message] # replaced by below
     @add_school.message = simple_format(@add_school.message)

    begin
      AddSchoolMailer.add_school_email(@add_school).deliver_now
#      flash.now[:success] = 'Thank you for your comment, ' + @contact_us.from_name + '. We\'ll contact you as soon as possible.'
    rescue Exception => e
      errors = [@contact_us.from_name + ', our email system is down at the moment. Please contact administrator@payitforward4edu.com to add your school to the system.']
      errors << e.message
      flash.now[:danger] = errors.join("<br>").html_safe
    end


    @contact_us.clear
    render 'new'

  end



  private # Internal only

  def add_school_params
    params.require(:add_school).permit(:from_name, :from_email, :subject, :message)
  end

end
