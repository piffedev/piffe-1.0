/**
 * Created by oleung on 5/9/15.
 */
/*
$('#title3rdGrade').click(function() {
    icon = $(this).find("i");
    icon.toggleClass("fa-chevron-circle-right fa-chevron-circle-down");
});
*/
//$(document).on('page:load', function() {
$(function() {

    $('#schedule-tab-content .panel-collapse').on('hide.bs.collapse', function (e) {
        icon = $(this).siblings('.panel-heading').find('i');
        icon.toggleClass("fa-plus-circle fa-minus-circle");

    });

    $('#schedule-tab-content .panel-collapse').on('show.bs.collapse', function (e) {
        icon = $(this).siblings('.panel-heading').find('i');
        icon.toggleClass("fa-plus-circle fa-minus-circle");

    });

})

// utcTime format: xx:xxz.  eg: 23:30z
// if today is standard time, add another hour (assuming the time is set for the FALL with daylight savings time)
function utcToLocal(utcTime) {
    var time = moment(utcTime, 'hh:mm Z');

    var today = new Date();
    if (!today.dst()) {
        time.hour(time.hour() + 1);
    }

    var formattedTime = time.format('hh:mma') + ' ' + getTimeZoneAbbreviation();
    return formattedTime;
}


// returns the timezone abbreviation on the client side (browser)
function getTimeZoneAbbreviation() {
    var tz = jstz.determine();
    return moment.tz.zone(tz.name()).abbr(new Date().getTime());
}

Date.prototype.stdTimezoneOffset = function() {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function() {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}